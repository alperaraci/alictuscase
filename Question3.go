package main

import (
		"fmt"
		"io/ioutil"
		"math"
		"os"
		"strconv"
		"runtime"
)



func main() {

	//For Parralelization
	numcpu := runtime.NumCPU()
	runtime.GOMAXPROCS(numcpu)

	seperateFile("C:/Users/Alper/Desktop/datasets/dateset_1/customer.tbl") //This file path must change for testing
	seperateFile("C:/Users/Alper/Desktop/datasets/dateset_1/orders.tbl")	//This file path must change for testing
	seperateFile("C:/Users/Alper/Desktop/datasets/dateset_1/lineitem.tbl")	//This file path must change for testing

		
}

func seperateFile(fileToBePartition string){

	file, err := os.Open(fileToBePartition)

	if err != nil {
			fmt.Println(err)
			os.Exit(1)
	}

	defer file.Close()

	fileInfo, _ := file.Stat()

	var fileSize int64 = fileInfo.Size()

	const fileChunk = 1 * (1 << 20) // 1 MB, change this to your requirement

	// calculate total number of parts the file will be chunked into
	totalPartsNum := uint64(math.Ceil(float64(fileSize) / float64(fileChunk)))

	fmt.Printf("Splitting to %d pieces.\n", totalPartsNum)

	for i := uint64(0); i < totalPartsNum; i++ {

			partSize := int(math.Min(fileChunk, float64(fileSize-int64(i*fileChunk))))
			partBuffer := make([]byte, partSize)

			file.Read(partBuffer)

			// Write File to Parh
			fileName := fileToBePartition + "_" + strconv.FormatUint(i, 10)
			_, err := os.Create(fileName)

			if err != nil {
					fmt.Println(err)
					os.Exit(1)
			}

			// write/save buffer to disk
			ioutil.WriteFile(fileName, partBuffer, os.ModeAppend)

			fmt.Println("Split to : ", fileName)
		
	}

}